
# -*- coding: utf-8 -*-
from database import Database


class Mark:
    def __init__(self, mark):
        self._name = mark.get('name', False)
        self._code = mark.get('code', False)

    @staticmethod
    def browse(mark_id):
        browse_mark_query = """select * from mark where id = {mark_id}""".format(mark_id=mark_id)
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_mark_query)
        mark = ps_cursor.fetchone()
        return mark

    @staticmethod
    def list_mark():
        browse_mark_query = """select * from mark """
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_mark_query)
        mark = ps_cursor.fetchall()
        return mark

    @property
    def name(self):
        return self._name

    @property
    def code(self):
        return self._code

    @name.setter
    def name(self, name):
        self._name = name

    @code.setter
    def code(self, code):
        self._code = code

    def __repr__(self):
        return "Marca" % self.name
