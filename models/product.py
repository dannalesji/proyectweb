# -*- coding: utf-8 -*-
from database import Database


class Product:
    def __init__(self, product):
        self._name = product.get('name', False)
        self._code = product.get('code', False)
        self._mark_id = product.get('mark_id', False)

    @staticmethod
    def browse(product_id):
        browse_product_query = """select * from product where product_id = {product_id}""".format(product_id=product_id)
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_product_query)
        product = ps_cursor.fetchone()
        return product

    @staticmethod
    def list_product():
        browse_product_query = """select * from product """
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_product_query)
        product = ps_cursor.fetchall()
        return product

    @staticmethod
    def browsebymark(mark_id):
        browse_product_query = """select * from product where mark_id = {mark_id}""".format(mark_id=mark_id)
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_product_query)
        product = ps_cursor.fetchall()
        return product


    def insert_product(sefl):
        browse_product_query = """ insert into product(name, code, mark_id) VALUES 
        ({name},{code},{mark_id})""".format(name=sefl.name, code=sefl.code, mark_id=sefl.mark_id)
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_product_query)
        product = ps_cursor.fetchall()
        return product

    @staticmethod
    def delete_product(product_id):
        browse_product_query = """DELETE FROM product WHERE product_id = {product_id}""".format(product_id=product_id)
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_product_query)
        product = ps_cursor.fetchone()
        return product


    def update_product(sefl, id??):
        browse_product_query = """ update product set ??name, code, mark_id) VALUES 
        ({name},{code},{mark_id})""".format(name=sefl.name, code=sefl.code, mark_id=sefl.mark_id)
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_product_query)
        product = ps_cursor.fetchall()
        return product
    @property
    def name(self):
        return self._name

    @property
    def code(self):
        return self._code

    @property
    def code(self):
        return self._mark_id

    @name.setter
    def name(self, name):
        self._name = name

    @code.setter
    def code(self, code):
        self._code = code

    @mark_id.setter
    def code(self, mark_id):
        self._mark_id = mark_id

    def __repr__(self):
        return "Product" % self.name
