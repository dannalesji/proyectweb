# -*- coding: utf-8 -*-
from database import Database


class Sale:
    def __init__(self, sale):
        self._total = sale.get('total', False)
        self._dates = sale.get('dates', False)

    def insert_sale(sefl):
        browse_sale_query = """ insert into sale(total, dates) VALUES 
           ({total},{dates})""".format(totals=sefl.total, dates=sefl.dates)
        db = Database()
        ps_connection = db.session()
        ps_cursor = ps_connection.cursor()
        ps_cursor.execute(browse_sale_query)
        sale = ps_cursor.fetchall()
        return sale

    @property
    def amount(self):
        return self._total

    @property
    def dates(self):
        return self._dates

    @total.setter
    def name(self, total):
        self._total = total

    @dates.setter
    def code(self, dates):
        self._dates = dates
