# -*- coding: utf-8 -*-
from flask import Flask, request
from models.product import Product

app = Flask(__name__)


@app.route("/")
def index():
    return "Welcome to Web!"


@app.route("/sistemaventa/search/<int:product_id>")
def search_product(product_id):
    product_model = Product({})
    product_data = product_model.browse(product_id)
    # return str(product_data[0]) + ' ' + product_data[1] + ' ' + product_data[2]
    return str(product_data)


@app.route("/sistemaventa/list_product")
def list_product():
    product_model = Product({})
    product_data = product_model.list_product()
    # return str(product_data[0]) + ' ' + product_data[1] + ' ' + product_data[2]
    return str(product_data)


@app.route("/sistemaventa/searchbymarca/<int:mark_id>")
def search_productbymark(mark_id):
    product_model = Product({})
    product_data = product_model.browsebymarca(mark_id)
    return str(product_data)

@app.route("/sistemaventa/insert", methods=['POST'])
def insert_product():
    product_model = Product(request.json)
    product_data = product_model.insert_product()
    return str(product_data)


@app.route("/sistemaventa/delete/<int:product_id>")
def delete_product(product_id):
    product_model = Product({})
    product_data = product_model.delete_product(product_id)
    # return str(product_data[0]) + ' ' + product_data[1] + ' ' + product_data[2]
    return str(product_data)


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=1368, debug=True)
