create table mark(
    mark_id serial primary key,
    name character varying,
    code character varying
);

create table product(
    product_id serial primary key,
    name character varying,
    code character varying,
    mark_id integer,
    foreign key (mark_id) REFERENCES mark(mark_id)
);

create table sale(
    sale_id serial primary key,
    totals decimal(2,1),
    dates date
);
create table saledetail(
    sale_id integer,
    product_id integer,
    price decimal(2,1),
    amount integer,
    foreign key (sale_id) REFERENCES sale(sale_id),
    foreign key (product_id) REFERENCES  product(product_id)
);


insert into mark(name, code) VALUES ('HP','m-001');
insert into mark(name, code) VALUES ('Epson','m-002');

insert into product(name, code, mark_id) VALUES ('NoteBook Pro','p-001',1);
insert into product(name, code, mark_id) VALUES ('Impresora InTank','p-002',2);
insert into product(name, code, mark_id) VALUES ('NoteBook Pavilion','p-003',1);
insert into product(name, code, mark_id) VALUES ('Impresora Laser 107w','p-004',2);



select * from product;
select * from mark;


drop table product;
drop  table mark;

--select p.* from product p inner join measure m on m.id = p.measure_id